<?php
namespace FormPluginTerms;

/**
 * Zda je pole povinné
 * Pokud se kladou nejaké podmínky a pole není povinné je potřeba requed dát na false. Pokud nejsou žádné podmínky na vstup, pak se bere jako, že není povinné
 * @param $value
 * @param $param
 * @return bool
 */
function requied($value, $param = true){
    if($param === false) return true;
    return $value !== null;
}

/**
 * Kontroluje datový typ
 * @param $value mixed Kontrolovaná hodnota
 * @param $param string Datový typ pro kontrolu
 * @return bool|null Bool pokud hodnota je/není datový typ rovna $param, Null pokud neuznam datový typ
 */
function type($value, $param){
    switch($param){
        case("string"): return is_string($value);
        case("numeric"): return is_numeric($value);
        case("array"): return is_array($value);
        case("photo"): return typePhoto($value);
        case("email"): return filter_var($value, FILTER_VALIDATE_EMAIL) ? true : false;
    }
    return null;
}

function typePhoto($value){
    return
        $value["error"] == 0 &&
        $value["size"] > 0 &&
        (
            $value["type"] == "image/png" ||
            $value["type"] == "image/jpeg"
        );
}

/**
 * Kontroluje hodnoty na typ uvnitř jednoho pole (1. urovně)
 * @param $value array Pole jejiž prvky kontroluji
 * @param $param string Datový typ pro kontrolu
 * @return bool Zda jsou všechny prvky $param datového typu
 */
function arrayType($value, $param){
    foreach ($value as $item){
        if(!type($item, $param)) return false;
    }
    return true;
}

/**
 * Zda je to číslo a zda je vetší rovno parametru
 * @param $value
 * @param $param
 * @return bool
 */
function min_val($value, $param){
    return type($value, "numeric") && $value >= $param;
}

/**
 * Alias pro min_val
 */
function min_value($value, $param){
    return min_val($value, $param);
}

/**
 * Zda je to číslo a zda je mensi rovno parametru
 * @param $value
 * @param $param
 * @return bool
 */
function max_val($value, $param){
    return type($value, "numeric") && $value <= $param;
}

/**
 * Alias pro max_val
 */
function max_value($value, $param){
    return min_val($value, $param);
}

/**
 * Zda je to string a zároven délka je vetsi rovno parametru
 * @param $value
 * @param $param
 * @return bool
 */
function min_len($value, $param){
    return type($value, "string") && strlen($value) >= $param;
}


/**
 * Porovnává pole na stejnosti jednotlivých itemů
 * @param $value array Pole hodnot. Pokud je to pole o velikosti 1, pak vždy true
 * @param $param bool Pokud je true, pak se všechny hodnoty můsí rovnat (např. heslo a kontrola hesla), Pokud je false pak se hodnoty nesmí rovnat
 * @return bool
 */
function equal($value, $param){
    if(!is_array($value)) return false;
    if(\count($value) == 1) return true;
    if($param){
        for ($i=0; $i<\count($value); $i++){
            for ($j=$i+1; $j<\count($value); $j++){
                if($value[$i]!==$value[$j]) return false;
            }
        }
    }else{
        for ($i=0; $i<\count($value); $i++){
            for ($j=$i+1; $j<\count($value); $j++){
                if($value[$i]===$value[$j]) return false;
            }
        }
    }
    return true;
}

/**
 * Kontrola na přesnou délku pole
 * @param $value array
 * @param $param int
 * @return bool
 */
function count($value, $param){
    return type($value, "array") && \count($value) == $param;
}

/**
 * Zda je to array a zároven délka je vetsi rovno parametru
 * @param $value
 * @param $param
 * @return bool
 */
function min_count($value, $param){
    return type($value, "array") && \count($value) >= $param;
}

/**
 * Alias pro min_count
 * @param $value array
 * @param $param int
 * @return bool
 */
function min_array($value, $param){
    return min_count($value, $param);
}


/**
 * Zda je to array a zároven délka je mensi rovno parametru
 * @param $value
 * @param $param
 * @return bool
 */
function max_count($value, $param){
    return type($value, "array") && \count($value) <= $param;
}

/**
 * Alias pro max_count
 * @param $value array
 * @param $param int
 * @return bool
 */
function max_array($value, $param){
    return max_count($value, $param);
}
