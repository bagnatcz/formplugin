<?php
namespace FormPluginFilters;

/**
 * Upraví text na povolený obsah - znefunkční html
 * @param $value string
 * @return string
 */
function user_text_content($value){
    return htmlspecialchars($value);
}

/**
 * Upraví text v poli na povolený obsah - znefunkční html v poli
 * @param $value array Pole stringů
 * @return mixed array
 */
function user_text_content_array($value){
    foreach ($value as &$v) $v = htmlspecialchars($v);
    return $value;
}

?>