<?php
use function FormPluginErrors\is_this_error; //pouze pokud chcete is_this_error funkci pouzit - is_tris_error($data, "input-name", "error-name"), vrací true/false

/**
 * Samotný formulář
 * @param $hidden string Skrytý vstup, který informuje plugin o odeslání formuláře (potřeba vypsat uvnitř formuláře)
 */
function form_example_echo($hidden)
{
    ?>
        <form> Formulář může být jak POST tak GET
            <input type='text' name='jmeno'> toto NEdostaneš v data v funkci save
            <input type='text' name='example-jmeno'> toto budeš mít v data['jmeno']
            <input type='text' name='example-mail'> toto budeš mít v data['mail']
            <input type='password' name='example-passwd[]'>
            <input type='password' name='example-passwd[]'>
        <input type='submit' name=''>
        {$hidden}
        </form>

    <?php
}

/**
 * Funkce, která se spastí, pokud formulář "example" byl odeslán
 * @param $data
 * @param $errors
 * @return bool
 */
function form_example_save($data, $errors)
{
    if($errors){
        // práce s formulářem, pokud byl vyplněn špatně
        return false;
    }else{
        // správně vyplněný formulář
        return true;
    }
}

/**
 * Přiřazení kontrol ke vstupům.
 * @return array
 */
function form_example_terms() //tímto lze nahradit defaultní podmínky v případě, že se žádne nepředají explicitně
{
    return array(
        "jmeno" => array(           //název inputu (v html to je name='example-jmeno')
            "requied" => true,      //je to vyžadováno (není potřeba uvádět, ty názvy, co v podmínkách nemají zastoupení jsou volitelné - např. name='example-mail' je volitelný)
            "min_len" => 3,         //minimální délka
            "type"    => "string"   //datový typ - string | numeric | array
            //v případě array se všechny min kontrolují pro všechny pole
        ),
        "passwd" => array(
            "min_count" => 2,
            "equal"     => true,
            "arrayType" => "string"
        )
    );
}

/**
 * Přiřazení filtrů ke vstupům
 * @return array
 */
function form_example_filters()
{
    return array(
        "jmeno" => array("user_text_content"), //aplikuje na vstup name filter user_text_content
    );
}




//---------Toto vložíte do stránky, kde chcete formulář-------------//

//vytvoření instance bez podmínek
$form = new Form("example");

{
    //vytvoření instance s explicitními podmínkami (nebere se v potaz form_example_terms)
    $podminky = array(
        "jmeno" => array(
            "requied" => false
        )
    );
    $form = new Form("example", $podminky);
}

//vypsání formuláře
$form->get_form();

//uložení formuláře
$form->save_form();

//dále lze formuláři předat data přes get_form, zatím není dokumentace
?>